﻿using System.IO;
using System.Reflection;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Pdf;
using Android.OS;
using Android.Widget;
using Java.IO;
using PdfRenderer;using PdfRenderer.Android.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Environment = System.Environment;
using File = Java.IO.File;
using ListView = Xamarin.Forms.ListView;
using Path = System.IO.Path;

[assembly: ExportRenderer(typeof(PdfView), typeof(PdfViewRenderer))]
namespace PdfRenderer.Android.Renderers
{
    public class PdfViewRenderer : ListViewRenderer
    {

        private readonly string _pdfPath;
        
        public PdfViewRenderer(Context context) : base(context)
        {
            _pdfPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "pdfToRender.pdf");
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;
            Control.DividerHeight = 20;
            
            using var pdfControl = GetPdfControl();
            for (var i = 0; i < pdfControl.PageCount; i++)
            {
                var imageView = new ImageView(Context);
                SavePdfPageToBitmap(pdfControl, imageView, i);
                Control.AddFooterView(imageView);
            }
            pdfControl.Close();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if(System.IO.File.Exists(_pdfPath)) System.IO.File.Delete(_pdfPath);
        }
        
        private void SavePdfPageToBitmap( global::Android.Graphics.Pdf.PdfRenderer pdfControl, ImageView imageView, int page)
        {
            if (page > pdfControl.PageCount - 1) throw new InvalidObjectException($"{nameof(page)} and {nameof(pdfControl.PageCount)} have incompatible values");
            
            var pdfPage = pdfControl.OpenPage(page);
            var screenBitmap = GetDisplaySizeBitmap(pdfPage.Height, pdfPage.Width);
            pdfPage.Render(screenBitmap, null, null, PdfRenderMode.ForDisplay);
            pdfPage.Close();
            imageView.SetImageBitmap(screenBitmap);
        }


        private Bitmap GetDisplaySizeBitmap(int pageHeight, int pageWidth)
        {
            var display = Resources.DisplayMetrics;
            var ratio = (float) pageHeight / pageWidth;
            var height = (int) (ratio * display.WidthPixels);
            
            return Bitmap.CreateBitmap(display.WidthPixels, height, Bitmap.Config.Argb8888);
        }
        
        private global::Android.Graphics.Pdf.PdfRenderer GetPdfControl()
        {
            var pdfFile = GetFile();
            var pdfDescriptor = new ParcelFileDescriptor(ParcelFileDescriptor.Open(pdfFile, ParcelFileMode.ReadOnly));
            var pdfControl = new global::Android.Graphics.Pdf.PdfRenderer(pdfDescriptor);
            return pdfControl;
        }

        private File GetFile()
        {
            var assembly = Assembly.GetExecutingAssembly();
            using var resourceStream = assembly.GetManifestResourceStream("PdfRenderer.Android.csharpRules.pdf");
            using var fileStream = new FileStream(_pdfPath, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
            resourceStream.CopyTo(fileStream);

            return new File(_pdfPath);
        }
    }
}